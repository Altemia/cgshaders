﻿//------------------------------------------------------------------
///
//------------------------------------------------------------------
Shader "Custom/oilRainnbowShad"
{
/*Properties{
		_RainBow("RainBow", 2D) = "white" {}
		_Noise("Noise", 2D) = "white" {}
		_Intensity("Intensity", Float) = 6
}
SubShader{
	Tags {
		"RenderType" = "Opaque"
	}
	Pass {
		Name "FORWARD"
		Tags {
			"LightMode" = "ForwardBase"
		}


		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#define UNITY_PASS_FORWARDBASE
		#include "UnityCG.cginc"
		#pragma multi_compile_fwdbase_fullshadows
		#pragma only_renderers d3d9 d3d11 glcore gles 
		#pragma target 3.0
		uniform sampler2D _RainBow; uniform float4 _RainBow_ST;
		uniform sampler2D _Noise; uniform float4 _Noise_ST;
		uniform float _Intensity;
		struct VertexInput
		{
			float4 vertex : POSITION;
			float3 normal : NORMAL;
			float2 texcoord0 : TEXCOORD0;
		};
		struct VertexOutput
		{
			float4 pos : SV_POSITION;
			float2 uv0 : TEXCOORD0;
			float4 posWorld : TEXCOORD1;
			float3 normalDir : TEXCOORD2;
		};
		VertexOutput vert(VertexInput v)
		{
			VertexOutput o = (VertexOutput)0;
			o.uv0 = v.texcoord0;
			o.normalDir = UnityObjectToWorldNormal(v.normal);
			o.posWorld = mul(unity_ObjectToWorld, v.vertex);
			o.pos = UnityObjectToClipPos(v.vertex);
			return o;
		}
		float4 frag(VertexOutput i) : COLOR
		{
			i.normalDir = normalize(i.normalDir);
			float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
			float3 normalDirection = i.normalDir;
			float4 _Noise_var = tex2D(_Noise,TRANSFORM_TEX(i.uv0, _Noise));
			float  NDotV = dot(normalDirection,viewDirection);
			float2 NDotV2 = ((_Noise_var.r*float2(NDotV,NDotV))*_Intensity);
			float4 tex = tex2D(_RainBow,TRANSFORM_TEX(NDotV2, _RainBow));
			float3 emissive = tex.rgb;
			float3 finalColor = emissive;
			return fixed4(finalColor,1);
		}
		ENDCG
	}
		}
			FallBack "Diffuse"
}*/

	/*Properties{
		[Space(20)][Header(MainTex and ColorRamp)][Space(20)]
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_ColorRamp("ColorRamp",2D) = "white"{}
		_Blend("Blend",Range(0,1)) = 0.5

		[Header(Mask)][Space(20)]
		_Mask("Mask",2D) = "white"{}

		[Space(20)][Header(Adding Distortion)][Space(20)]
		_Noise("Noise", 2D) = "white" {}
		_Distortion("Distortion",Float) = 6

		[Space(20)][Header(BumpMap and BumpPower)][Space(20)]

		_BumpMap("Bumpmap", 2D) = "bump" {}
		_BumpPower("BumpPower",Range(0.1,1)) = 0.1

		[Space(20)][Header(Change ColorRamp)][Space(20)]
		_Hue("Hue", Range(0, 1.0)) = 0
		_Saturation("Saturation", Range(0, 1.0)) = 0.5
		_Brightness("Brightness", Range(0, 1.0)) = 0.5
		_Contrast("Contrast", Range(0, 1.0)) = 0.5

		[Space(20)][Header(Smoothness and Metallic)][Space(20)]
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
	}
		SubShader{
			Tags { "RenderType" = "Opaque" }
			LOD 200

			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			#pragma surface surf Standard fullforwardshadows

			// Use shader model 3.0 target, to get nicer looking lighting
			#pragma target 3.0

		  sampler2D _MainTex,_ColorRamp,_Noise,_Mask;
		  sampler2D _BumpMap;
		  float4 _RimColor;
		  float _BumpIntensity;

			struct Input {
			  float2 uv_MainTex;
			  float2 uv_Mask;
			  float2 uv_BumpMap;
			  float2 uv_ColorRamp;
			  float3 viewDir;
			  float3 worldPos;
			};

		  float4 _ColorRamp_ST;
			half _Glossiness;
			half _Metallic;
			float _Blend;
			float _BumpPower;
			float _Distortion;

			UNITY_INSTANCING_BUFFER_START(Props)
				// put more per-instance properties here
			UNITY_INSTANCING_BUFFER_END(Props)

			fixed _Hue, _Saturation, _Brightness, _Contrast;


						inline float3 applyHue(float3 aColor, float aHue)
				{
					float angle = radians(aHue);
					float3 k = float3(0.57735, 0.57735, 0.57735);
					float cosAngle = cos(angle);

					return aColor * cosAngle + cross(k, aColor) * sin(angle) + k * dot(k, aColor) * (1 - cosAngle);
				}

				inline float4 applyHSBCEffect(float4 startColor, fixed4 hsbc)
				{
					float hue = 360 * hsbc.r;
					float saturation = hsbc.g * 2;
					float brightness = hsbc.b * 2 - 1;
					float contrast = hsbc.a * 2;

					float4 outputColor = startColor;
					outputColor.rgb = applyHue(outputColor.rgb, hue);
					outputColor.rgb = (outputColor.rgb - 0.5f) * contrast + 0.5f;
					outputColor.rgb = outputColor.rgb + brightness;
					float3 intensity = dot(outputColor.rgb, float3(0.39, 0.59, 0.11));
					outputColor.rgb = lerp(intensity, outputColor.rgb, saturation);

					return outputColor;
				}


			void surf(Input IN, inout SurfaceOutputStandard o) {
				// Albedo comes from a texture tinted by color
				float4 c = tex2D(_MainTex, IN.uv_MainTex);
				float noise = tex2D(_Noise,IN.uv_MainTex);
				fixed3 normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
				normal.z /= _BumpPower;
				o.Normal = normalize(normal);
				float2 rim = dot(normalize(IN.viewDir), o.Normal);
				float2 distortion = noise * _Distortion;
				//float2 rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));

				float4 mask = tex2D(_Mask,IN.uv_Mask);

				float4 colorRamp = tex2D(_ColorRamp,TRANSFORM_TEX(rim, _ColorRamp)*distortion)*mask;
				colorRamp = max(colorRamp,(1 - mask)* c);

				fixed4 hsbc = fixed4(_Hue, _Saturation, _Brightness, _Contrast);
				float4 colorRampHSBC = applyHSBCEffect(colorRamp, hsbc);
				o.Albedo = lerp(c,colorRampHSBC,_Blend);
				o.Metallic = _Metallic;
				o.Smoothness = _Glossiness;
				o.Alpha = c.a;
			}
			ENDCG
		}
			FallBack "Diffuse"
}*/


	Properties
	{
		_ColourSpectrum("_ColourSpectrum", 2D) = "white" {}

		_Color("_Color", Color) = (1,1,1,1)
		_MainTex("_MainTex", 2D) = "white" {}

		_OilColor("_OilColor", Color) = (1,1,1,1)
		_NoiseTex("_NoiseTex", 2D) = "white" {}

		_OilSpeed("_OilSpeed 1(x, y) 2(z, w)", Vector) = (0.5,0.5,0.5,1.0)
	}

	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"RenderType" = "Transparent"
		}

		//Cull Off
		ZWrite On
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float3 normal	: NORMAL;

				float2 texcoord : TEXCOORD0;
				float2 bumpTexCoords : TEXCOORD1;

				half3 tspace0 : TEXCOORD2; // tangent.x, bitangent.x, normal.x
				half3 tspace1 : TEXCOORD3; // tangent.y, bitangent.y, normal.y
				half3 tspace2 : COLOR; // tangent.z, bitangent.z, normal.z

				float3 viewDir : TANGENT;
			};

			sampler2D _ColourSpectrum;
			float4 _ColourSpectrum_ST;

			sampler2D _MainTex;
			float4 _MainTex_ST;

			sampler2D _BumpMap;
			float4 _BumpMap_ST;

			sampler2D _NoiseTex;
			float4 _NoiseTex_ST;

			fixed4 _Color;
			fixed4 _OilColor;

			float4 _OilSpeed;

			sampler2D _CameraDepthTexture; // automatically set up by Unity. Contains the scene's depth buffer

			//------------------------------------------------------------------
			v2f vert(appdata_full IN)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(IN.vertex);
				o.normal = normalize(UnityObjectToWorldNormal(IN.normal));

				o.texcoord = TRANSFORM_TEX(IN.texcoord, _MainTex);
				o.bumpTexCoords = TRANSFORM_TEX(IN.texcoord, _BumpMap);

				//-- Normals
				half3 wNormal = UnityObjectToWorldNormal(IN.normal);
				half3 wTangent = UnityObjectToWorldDir(IN.tangent.xyz);
				// compute bitangent from cross product of normal and tangent
				half tangentSign = IN.tangent.w * unity_WorldTransformParams.w;
				half3 wBitangent = cross(wNormal, wTangent) * tangentSign;
				// output the tangent space matrix
				o.tspace0 = half3(wTangent.x, wBitangent.x, wNormal.x);
				o.tspace1 = half3(wTangent.y, wBitangent.y, wNormal.y);
				o.tspace2 = half3(wTangent.z, wBitangent.z, wNormal.z);

				//-- View Direction
				float3 vertexWorldPosition = mul(unity_ObjectToWorld, IN.vertex).xyz;
				float3 objectWorldPosition = mul(unity_ObjectToWorld, float4(0, 0, 0, 1)).xyz;
				o.viewDir = normalize(vertexWorldPosition - _WorldSpaceCameraPos);

				return o;
			}

			//------------------------------------------------------------------
			float4 GetOilColour(v2f IN)
			{
				float4 result = float4(1.0f, 1.0f, 1.0f, 1.0f);

				float _Intensity = 6.0f;

				float4 _NoiseSample = tex2D(_NoiseTex, TRANSFORM_TEX(IN.texcoord, _NoiseTex));

				float  NDotV = dot(IN.normal, IN.viewDir);

				float2 NDotV2 = ((_NoiseSample.r * float2(NDotV, NDotV)) * _Intensity);

				float4 colorSample = tex2D(_ColourSpectrum, TRANSFORM_TEX(NDotV2, _ColourSpectrum));

				float3 finalColor = colorSample.rgb;
				
				result.rgb = colorSample.rgb;

				return result;
			}

			//------------------------------------------------------------------
			fixed4 frag(v2f IN) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, IN.texcoord);
				col *= _Color;

				float2 coordinates = IN.texcoord + (_Time.y * _OilSpeed.xy * 0.05f);
				fixed4 noisePass1 = tex2D(_NoiseTex, coordinates);

				coordinates += _Time.y * _OilSpeed.zw * 0.05f;
				fixed4 noisePass2 = tex2D(_NoiseTex, coordinates);

				fixed4 noisePass = (noisePass1 + noisePass2) / 2.0f;

				float grayScale = (noisePass.r + noisePass.g + noisePass.b) / 3.0f;
				if (grayScale < 0.7f)
					grayScale = 0.0f;

				float4 _LightDir = float4(0.1f, -1.0f, 0.1f, 1.0f);
				//-- Bump Normals
				float3 bumpNormals = UnpackNormal(tex2D(_BumpMap, IN.bumpTexCoords)).rgb;
				// transform normal from tangent to world space
				half3 worldNormal;
				worldNormal.x = dot(IN.tspace0, bumpNormals);
				worldNormal.y = dot(IN.tspace1, bumpNormals);
				worldNormal.z = dot(IN.tspace2, bumpNormals);
				//compute Lambertian diffuse coefficient from the normal and light vectors
				float Kd = 1.0f;// saturate(dot(_LightDir.xyz, worldNormal) * 20.2f);

				float4 oilColor = GetOilColour(IN) * Kd;
				col.rgb = lerp(col.rgb, oilColor.rgb, grayScale);

				return col;
			}
		ENDCG
		}
	}
}