﻿//------------------------------------------------------------------
/// billboard Surface Shader.
//------------------------------------------------------------------
Shader "custom/billboardSurfaceShader" 
{
	Properties
	{
	[TCP2HeaderHelp(BASE, Base Properties)]
		//TOONY COLORS
		_Color("Color", Color) = (1,1,1,1)
		_HColor("Highlight Color", Color) = (0.785,0.785,0.785,1.0)
		_SColor("Shadow Color", Color) = (0.195,0.195,0.195,1.0)

		//DIFFUSE
		_MainTex("Main Texture", 2D) = "white" {}
	[TCP2Separator]

		//TOONY COLORS RAMP
	[TCP2Header(RAMP SETTINGS)]

		_RampThreshold("Ramp Threshold", Range(0,1)) = 0.5
		_RampSmooth("Ramp Smoothing", Range(0.001,1)) = 0.1
	[TCP2Separator]

	[TCP2HeaderHelp(TRANSPARENCY)]

	//Alpha Testing
	_Cutoff("Alpha cutoff", Range(0,1)) = 0.5
[TCP2Separator]


	//Avoid compile error if the properties are ending with a drawer
	[HideInInspector] __dummy__("unused", Float) = 0
	}

	SubShader
	{
		Tags
		{
			"Queue" = "AlphaTest"
			"IgnoreProjector" = "True"
			"RenderType" = "TransparentCutout"
			"ForceNoShadowCasting" = "True"
		}
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf ToonyColorsCustom addshadow exclude_path:deferred exclude_path:prepass vertex:vert
		
		#pragma target 2.5

		//------------------------------------------------------------------
		/// Variables.
		//------------------------------------------------------------------
		// User-specified uniforms            
		uniform sampler2D _MainTex;
		fixed4 _Color;
		fixed _Cutoff;

		#define UV_MAINTEX uv_MainTex

		struct Input
		{
			half2 uv_MainTex;
		};

		//================================================================
		// CUSTOM LIGHTING

		//Lighting-related variables
		fixed4 _HColor;
		fixed4 _SColor;
		half _RampThreshold;
		half _RampSmooth;

		// Instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
		// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		//Custom SurfaceOutput
		struct SurfaceOutputCustom
		{
			half atten;
			fixed3 Albedo;
			fixed3 Normal;
			fixed3 Emission;
			half Specular;
			fixed Gloss;
			fixed Alpha;
		};

		inline half4 LightingToonyColorsCustom(inout SurfaceOutputCustom s, half3 viewDir, UnityGI gi)
		{
#define IN_NORMAL s.Normal

			half3 lightDir = gi.light.dir;
#if defined(UNITY_PASS_FORWARDBASE)
			half3 lightColor = _LightColor0.rgb;
			half atten = s.atten;
#else
			half3 lightColor = gi.light.color.rgb;
			half atten = 1;
#endif

			IN_NORMAL = normalize(IN_NORMAL);
			fixed ndl = max(0, dot(IN_NORMAL, lightDir));
#define NDL ndl

#define		RAMP_THRESHOLD	_RampThreshold
#define		RAMP_SMOOTH		_RampSmooth

			fixed3 ramp = smoothstep(RAMP_THRESHOLD - RAMP_SMOOTH * 0.5, RAMP_THRESHOLD + RAMP_SMOOTH * 0.5, NDL);
#if !(POINT) && !(SPOT)
			ramp *= atten;
#endif
#if !defined(UNITY_PASS_FORWARDBASE)
			_SColor = fixed4(0, 0, 0, 1);
#endif
			_SColor = lerp(_HColor, _SColor, _SColor.a);	//Shadows intensity through alpha
			ramp = lerp(_SColor.rgb, _HColor.rgb, ramp);
			fixed4 c;
			c.rgb = s.Albedo * lightColor.rgb * ramp;
			c.a = s.Alpha;

#ifdef UNITY_LIGHT_FUNCTION_APPLY_INDIRECT
			c.rgb += s.Albedo * gi.indirect.diffuse;
#endif


			return c;
		}

		void LightingToonyColorsCustom_GI(inout SurfaceOutputCustom s, UnityGIInput data, inout UnityGI gi)
		{
			gi = UnityGlobalIllumination(data, 1.0, IN_NORMAL);

			s.atten = data.atten;	//transfer attenuation to lighting function
			gi.light.color = _LightColor0.rgb;	//remove attenuation
		}

		//------------------------------------------------------------------
		/// Get Rotation Matrix.
		//------------------------------------------------------------------
		float2x2 YRotationMatrix(float degrees)
		{
			float alpha = degrees * UNITY_PI / 180.0;
			float sina, cosa;
			sincos(alpha, sina, cosa);

			/*return float3x3(cosa, 0, -sina,
							0, 1, 0,
							sina, 0, cosa);*/

			return float2x2(cosa, -sina, sina, cosa);
		}

		//------------------------------------------------------------------
		/// Rotate vertex by Angle.
		//------------------------------------------------------------------
		float3 RotateVertex(float4 vertex, float in_degrees)
		{
			float4 result = vertex;

			result.xyz = float4(mul(YRotationMatrix(in_degrees), vertex.xz), vertex.yw).xzy;

			return result.xyz;
		}

		//------------------------------------------------------------------
		/// Vertex method.
		//------------------------------------------------------------------
		void vert(inout appdata_full input)
		{
			//-- Billboard
			float3 cameraWorldPos = _WorldSpaceCameraPos;
			cameraWorldPos.y = 0.0f;
			float3 objectWorldPos = mul(unity_ObjectToWorld, float4(0, 0, 0, 1));
			objectWorldPos.y = 0.0f;

			float3 objectToCameraDir = cameraWorldPos - objectWorldPos;

			float3 b = normalize(objectToCameraDir);
			float3 a = float3(0.0f, 0.0f, 1.0f);
			float angleRads = atan2(b.z, b.x) - atan2(a.z, a.x);
			float Rad2Deg = 57.29578f;
			float angleDegs = angleRads * Rad2Deg;

			//-- Rotate as a billboard
			input.vertex.xyz = RotateVertex(input.vertex, angleDegs);
		}

		void surf(Input IN, inout SurfaceOutputCustom o)
		{
			fixed4 mainTex = tex2D(_MainTex, IN.UV_MAINTEX);
			o.Albedo = mainTex.rgb * _Color.rgb;
			o.Alpha = mainTex.a * _Color.a;

			//Cutout (Alpha Testing)
			clip(o.Alpha - _Cutoff);
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "TCP2_MaterialInspector_SG"
}