﻿//------------------------------------------------------------------
/// billboard Surface Shader.
//------------------------------------------------------------------
Shader "Custom/billboardSurfaceShad" 
{
	Properties 
	{
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}

		_WindPower("Wind Power", Vector) = (1,1,1,1)
	}
	SubShader 
	{
		Tags
		{
			"Queue" = "AlphaTest"
			"IgnoreProjector" = "True" 
			"RenderType" = "TransparentCutout" 
			"ForceNoShadowCasting" = "True"
		}
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard addshadow exclude_path:deferred exclude_path:prepass vertex:vert

		#pragma target 2.5

		//------------------------------------------------------------------
		/// Variables.
		//------------------------------------------------------------------
		fixed4 _Color;
		sampler2D _MainTex;
		fixed _Cutoff;
		fixed4 _WindPower;

		#define UV_MAINTEX uv_MainTex

		struct Input
		{
			half2 uv_MainTex;
		};

		//================================================================
		// CUSTOM LIGHTING

		//Lighting-related variables
		fixed4 _HColor;
		fixed4 _SColor;
		half _RampThreshold;
		half _RampSmooth;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

			//------------------------------------------------------------------
			/// Get Rotation Matrix.
			//------------------------------------------------------------------
			float2x2 YRotationMatrix(float degrees)
		{
			float alpha = degrees * UNITY_PI / 180.0;
			float sina, cosa;
			sincos(alpha, sina, cosa);

			/*return float3x3(cosa, 0, -sina,
							0, 1, 0,
							sina, 0, cosa);*/

			return float2x2(cosa, -sina, sina, cosa);
		}

		//------------------------------------------------------------------
		/// Rotate vertex by Angle.
		//------------------------------------------------------------------
		float3 RotateVertex(float4 vertex, float in_degrees)
		{
			float4 result = vertex;

			result.xyz = float4(mul(YRotationMatrix(in_degrees), vertex.xz), vertex.yw).xzy;
			//input.vertex.xyz = mul(YRotationMatrix(in_degrees), input.vertex.xyz);

			return result.xyz;
		}

		//------------------------------------------------------------------
		/// Apply Sway.
		//------------------------------------------------------------------
		float3 ApplySway(float4 vertex, float2 in_texCoords)
		{
			float4 result = vertex;

			float3 speed = _WindPower.xyz;
			float yConstraint = in_texCoords.y;

			float amplitude = 0.25f;
			float frequency = 1.75f;

			float xPower = speed.x * yConstraint * (amplitude * cos(frequency * _Time.y));
			result.x += xPower;

			//float zPower = speed.z * yConstraint * (amplitude * sin(frequency * _Time.y));
			//result.z += zPower;

			return result.xyz;
		}

		//------------------------------------------------------------------
		/// Vertex.
		//------------------------------------------------------------------
		void vert(inout appdata_full input)
		{
			//-- Apply Sway
			input.vertex.xyz = ApplySway(input.vertex, input.texcoord);

			//-- Billboard
			float3 cameraWorldPos = _WorldSpaceCameraPos;
			cameraWorldPos.y = 0.0f;
			float3 objectWorldPos = mul(unity_ObjectToWorld, float4(0, 0, 0, 1));
			objectWorldPos.y = 0.0f;

			float3 objectToCameraDir = cameraWorldPos - objectWorldPos;

			float3 b = normalize(objectToCameraDir);
			float3 a = float3(0.0f, 0.0f, 1.0f);
			float angleRads = atan2(b.z, b.x) - atan2(a.z, a.x);
			float Rad2Deg = 57.29578f;
			float angleDegs = angleRads * Rad2Deg;

			//-- Rotate as a billboard
			input.vertex.xyz = RotateVertex(input.vertex, angleDegs);
		}

		//------------------------------------------------------------------
		/// Surface.
		//------------------------------------------------------------------
		void surf (Input IN, inout SurfaceOutputStandard o)
		{
			fixed4 mainTex = tex2D(_MainTex, IN.UV_MAINTEX);
			o.Albedo = mainTex.rgb * _Color.rgb;
			o.Alpha = mainTex.a * _Color.a;

			//Cutout (Alpha Testing)
			clip(o.Alpha - _Cutoff);
		}
		ENDCG
	}
	FallBack "Diffuse"
}
