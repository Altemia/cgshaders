﻿Shader "Custom/northernLightSurfaceShad" 
{
	Properties 
	{
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}

		_StartColor("Start Color", Color) = (1,1,1,1)
		_EndColor("End Color", Color) = (1,1,1,1)
	}
	SubShader 
	{
		Tags { "Queue" = "AlphaTest" "RenderType" = "TransparentCutout" "IgnoreProjector" = "True" "ForceNoShadowCasting" = "True" }
		LOD 200

		Cull Off
		ZWrite Off

		Blend SrcAlpha OneMinusSrcAlpha // Traditional transparency

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			float4 _StartColor;
			float4 _EndColor;

			float3 ApplyWave(float4 vertex, float3 in_normal)
			{
				float3 result = vertex;
				
				float amplitude = 1.25f;
				float frequency = result.x * 0.4f;

				result.z = (amplitude * sin(frequency + _Time.y));

				return result;
			}

			v2f vert(appdata_full v)
			{
				v2f o;

				v.vertex.xyz = ApplyWave(v.vertex, v.normal);

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);

				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);

				float3 newColour = lerp(_StartColor.xyz, _EndColor.xyz, i.uv.y);

				//-- Vertical Alpha.
				float mixValue = i.uv.y;
				float endAlpha = lerp(0.0f, 1.0f, 1.0f - mixValue);
				mixValue = i.uv.y;
				float alpha = lerp(0.0f, endAlpha, mixValue);

				//-- horizontal Alpha.
				float startOffset = 0.2f;
				float endOffset = 1.0f - startOffset;
				alpha *= max(min(lerp(0.0f, 1.0f, (i.uv.x / startOffset)), 1.0f), 0.0f);
				if (i.uv.x >= endOffset)
				{
					alpha *= max(lerp(1.0f, 0.0f, ((i.uv.x - endOffset) / startOffset)), 0.0f);
				}

				float4 result = float4(newColour.x, newColour.y, newColour.z, alpha);
				return result;
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}
