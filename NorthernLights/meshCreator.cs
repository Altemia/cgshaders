﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class meshCreator : MonoBehaviour
{
    public MeshFilter meshFilter = null;

    Mesh newMesh = null;

	// Use this for initialization
	void Start ()
    {
        newMesh = new Mesh();

        int width = 200;
        int height = 10;

        Vector3[] vertices = null;

        vertices = new Vector3[(width + 1) * (height + 1)];
        for (int i = 0, y = 0; y <= height; y++)
        {
            for (int x = 0; x <= width; x++, i++)
            {
                vertices[i] = new Vector3(x * 0.5f, y, 0.0f);
            }
        }

        int[] triangles = new int[width * height * 6];
        for (int ti = 0, vi = 0, y = 0; y < height; y++, vi++)
        {
            for (int x = 0; x < width; x++, ti += 6, vi++)
            {
                triangles[ti] = vi;
                triangles[ti + 3] = triangles[ti + 2] = vi + 1;
                triangles[ti + 4] = triangles[ti + 1] = vi + width + 1;
                triangles[ti + 5] = vi + width + 2;
            }
        }

        int widthUV = (int)(width * 1.0f);
        int heightUV = height;
        Vector2[] myUVs = new Vector2[vertices.Length]; // Create array with the same element count
        //for (var i = 0; i < vertices.Length; ++i)
        //{
        //    myUVs[i] = new Vector2(vertices[i].x / (float)widthUV, vertices[i].y / (float)heightUV);
        //    Debug.Log("UV: " + myUVs[i]);
        //}
        for (int i = 0, y = 0; y <= height; y++)
        {
            for (int x = 0; x <= width; x++, i++)
            {
                myUVs[i] = new Vector2((float)x / (float)widthUV, (float)y / (float)heightUV);
                Debug.Log("UV: " + myUVs[i]);
            }
        }

        newMesh.vertices = vertices;
        newMesh.triangles = triangles;
        newMesh.uv = myUVs;
        newMesh.RecalculateNormals();

        meshFilter.mesh = newMesh;
    }
}
