﻿//------------------------------------------------------------------
/// billboard Surface Shader.
//------------------------------------------------------------------
Shader "Custom/smokeParticleShad" 
{
	Properties 
	{
		_Color ("Color", Color) = (1,1,1,1)
		_ObjectColor("_ObjectColor", Color) = (1,1,1,1)
		_ObjectColorDark("_ObjectColorDark", Color) = (1,1,1,1)

		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_NoiseTex("_NoiseTex", 2D) = "white" {}

		_SpecularPower ("_SpecularPower", float) = 1
		_Gloss ("_Gloss", float) = 1

		_CloudSpeed("Cloud Speed 1(x, y) 2(z, w)", Vector) = (0.5,0.5,0.5,1.0)
	}
	SubShader 
		{
			Tags
			{
				"Queue" = "Transparent"
				"IgnoreProjector" = "True" 
				"RenderType" = "Transparent" 
				"ForceNoShadowCasting" = "True"
			}
			LOD 200

			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha

			Pass
			{
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"
				#include "Lighting.cginc"

			#define MAX_FLOAT 999999

			//------------------------------------------------------------------
			/// Data Container for vertex information.
			//------------------------------------------------------------------
				struct v2f
				{
					float2 uv : TEXCOORD0;
					float4 vertex : SV_POSITION;
					float3 normal : NORMAL;

					float3 objectWorldPosition : TEXCOORD1;
					float3 vertexWorldPosition : TEXCOORD2;

					float3 viewDir : TANGENT;
				};

				sampler2D _MainTex;
				float4 _MainTex_ST;

				sampler2D _NoiseTex;
				float4 _NoiseTex_ST;

				float4 _Color;
				float4 _ObjectColor;
				float4 _ObjectColorDark;

				float _SpecularPower;
				float _Gloss;

				float4 _CloudSpeed;

				//------------------------------------------------------------------
				///
				//------------------------------------------------------------------
				v2f vert(appdata_full v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);

					o.vertexWorldPosition = mul(unity_ObjectToWorld, v.vertex).xyz;
					o.objectWorldPosition = mul(unity_ObjectToWorld, float4(0,0,0,1)).xyz;

					o.viewDir = normalize(o.vertexWorldPosition - _WorldSpaceCameraPos);

					return o;
				}

#define STEPS 500
#define STEP_SIZE 0.5//0.005

				//-- This is the gray scale value of the noise texture. Smaller a value means shorter clouds (Darker on Texture).
#define MINIMUM_GRAY_SCALE 0.0f

#define SUN_POSITION float3(0.0f, 20.0f, 0.0f)

				float rand(float co)
				{
					return frac(sin(dot(co, float3(12.9898, 78.233, 45.5432))) * 43758.5453);
				}

				//------------------------------------------------------------------
				/// Sample texture to find strength of cloud
				//------------------------------------------------------------------
				float sampleTexture(float3 p)
				{
					float worldSize = 30.0f;

					float textureSize = worldSize * 12.0f;//float textureSize = 170.6f;
					float singleIncrement = worldSize;

					float yStep = p.y * 5.0f;// *0.0f;

					int xHeightMultiplier = yStep % 12;
					int zHeightMultiplier = yStep / 12;

					p.xz += float2(singleIncrement * xHeightMultiplier, singleIncrement * zHeightMultiplier);

					float2 uv = float2((p.x / textureSize), -(p.z / textureSize));

					float2 coordinates = float2(uv.x, uv.y);
					//coordinates += _Time.y * _CloudSpeed.xy * 0.05f ;
					float4 textureSample1 = tex2D(_NoiseTex, coordinates);

					coordinates = float2((p.x / worldSize), (p.y / worldSize));
					coordinates += _Time.y * _CloudSpeed.zw * 0.05f ;
					float4 textureSample2 = tex2D(_NoiseTex, coordinates);

					float4 result = textureSample1;// (textureSample1 + textureSample2) / 2.0f;
					//-- Take average colour intensity (Gray Scale)
					return (result.r + result.g + result.b) / 3.0f;
				}

				//------------------------------------------------------------------
				/// Returns float2 containing:
				/// X - The relative height of the cloud.
				/// Y - The Gray Scale value.
				//------------------------------------------------------------------
				float2 raymarchHit(float3 _Centre, float3 position, float3 direction)
				{
					for (int i = 0; i < STEPS; ++i)
					{
						float distance = sampleTexture(position);

						if (distance > MINIMUM_GRAY_SCALE)
						{
							float relativeHeight = position.y - _Centre.y;
							return float2(relativeHeight, distance);
						}

						position += direction * STEP_SIZE;
					}

					return float2(MAX_FLOAT, MAX_FLOAT);
				}

				//------------------------------------------------------------------
				///
				//------------------------------------------------------------------
				fixed4 simpleLambert(fixed3 normal, float3 viewDirection)
				{
					fixed3 lightDir = _WorldSpaceLightPos0.xyz;	// Light direction
					fixed3 lightCol = _LightColor0.rgb;		// Light color

					fixed4 diffuse = max(dot(normal, lightDir), 0.0f);

					//fixed NdotL = max(dot(normal, lightDir), 0);
					fixed4 c;
					//// Specular
					//fixed3 h = (lightDir - viewDirection) / 2.;
					//fixed s = pow(dot(normal, h), _SpecularPower) * _Gloss;
					//c.rgb = _ObjectColor * lightCol * NdotL + s;

					c.rgb = diffuse * lightCol;
					c.a = 1;

					return c;
				}
				//------------------------------------------------------------------
				///
				//------------------------------------------------------------------
				float3 normal(float3 pos)
				{
					//Find the normal at this point
					const fixed2 eps = fixed2(0.00, 0.05);

					//Can approximate the surface normal using what is known as the gradient. 
					//The gradient of a scalar field is a vector, pointing in the direction where the field 
					//increases or decreases the most.
					//The gradient can be approximated by numerical differentation
					return normalize(float3(
						sampleTexture(pos + eps.yxx) - sampleTexture(pos - eps.yxx),
						sampleTexture(pos + eps.xyx) - sampleTexture(pos - eps.xyx),
						sampleTexture(pos + eps.xxy) - sampleTexture(pos - eps.xxy)));
				}
				//------------------------------------------------------------------
				///
				//------------------------------------------------------------------
				fixed4 renderSurface(float3 p, float3 viewDirection, float3 _Centre)
				{
					fixed3 lightDir = normalize(_WorldSpaceLightPos0.xyz);	// Light direction
					float3 sunPosition = p + (lightDir * -1) * 10.0f;
					float3 sunDirection = normalize(lightDir * -1);

					float3 n = normal(p);
					return simpleLambert(n, viewDirection);
				}

				//------------------------------------------------------------------
				///
				//------------------------------------------------------------------
				fixed4 frag(v2f i) : SV_Target
				{
					//-- Sample the texture
					fixed4 col = tex2D(_MainTex, i.uv);

					float2 rayMarchResult = raymarchHit(i.objectWorldPosition, i.vertexWorldPosition, i.viewDir);
					float grayScaleValue = rayMarchResult.y;

					if (grayScaleValue != MAX_FLOAT)
					{
						float relativeHeight = rayMarchResult.x;

						float interpolate = min(max(grayScaleValue / 1.0f, 0.0f), 1.0f);

						//-- Colouring
						col = lerp(_ObjectColorDark, _ObjectColor, interpolate);

						//-- Alpha
						interpolate = min(max(grayScaleValue / MINIMUM_GRAY_SCALE, 0.0f), 1.0f);
						//col.a = lerp(1.0f, 0.0f, interpolate);

						//-- Lighting addition.
						float3 newPosition = i.vertexWorldPosition + float3(0, relativeHeight, 0);
						//col *= renderSurface(newPosition, i.viewDir, i.objectWorldPosition);
						return col;
					}
					else
					{
						col *= _Color;
						return col;
					}
				}
			ENDCG
			}
	}
}
