﻿//------------------------------------------------------------------
/// billboard Surface Shader.
//------------------------------------------------------------------
Shader "Custom/smokeParticleShad" 
{
	Properties 
	{
		_Color ("Color", Color) = (1,1,1,1)
		_ObjectColor("_ObjectColor", Color) = (1,1,1,1)
		_ObjectColorDark("_ObjectColorDark", Color) = (1,1,1,1)

		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_NoiseTex("_NoiseTex", 2D) = "white" {}

		_SpecularPower ("_SpecularPower", float) = 1
		_Gloss ("_Gloss", float) = 1

		_CloudSpeed("Cloud Speed 1(x, y) 2(z, w)", Vector) = (0.5,0.5,0.5,1.0)
	}
	SubShader 
		{
			Tags
			{
				"Queue" = "AlphaTest"
				"IgnoreProjector" = "True" 
				"RenderType" = "TransparentCutout" 
				"ForceNoShadowCasting" = "True"
			}
			LOD 200

			Blend SrcAlpha OneMinusSrcAlpha // Traditional transparency

			Pass
			{
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"
				#include "Lighting.cginc"

			#define MAX_FLOAT 999999

				struct v2f
				{
					float2 uv : TEXCOORD0;
					float4 vertex : SV_POSITION;

					float3 objectWorldPosition : TEXCOORD1;
					float3 vertexWorldPosition : TEXCOORD2;

					float3 viewDir : TANGENT;
				};

				sampler2D _MainTex;
				float4 _MainTex_ST;

				sampler2D _NoiseTex;
				float4 _NoiseTex_ST;

				float4 _Color;
				float4 _ObjectColor;
				float4 _ObjectColorDark;

				float _SpecularPower;
				float _Gloss;

				float4 _CloudSpeed;

				//------------------------------------------------------------------
				///
				//------------------------------------------------------------------
				v2f vert(appdata_full v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);

					o.vertexWorldPosition = mul(unity_ObjectToWorld, v.vertex).xyz;
					o.objectWorldPosition = mul(unity_ObjectToWorld, float4(0,0,0,1)).xyz;

					o.viewDir = normalize(o.vertexWorldPosition - _WorldSpaceCameraPos);

					return o;
				}

#define STEPS 400
#define STEP_SIZE 0.005
				////------------------------------------------------------------------
				//float displacement(float3 p, float _Radius)
				//{
				//	//p += sin(_Time.y);
				//	float frequency = 2.0f;
				//	float amplitude = 1.0f;

				//	return 0.0f;//(amplitude * sin(frequency * _Time.y)) + amplitude + _Radius;//sin(frequency * p.x)*sin(frequency * p.y)*sin(frequency * p.z);
				//}

				//float Shape(float3 p, float3 _Centre, float _Radius)
				//{
				//	float h = _Radius * 1.0f;

				//	p.y -= clamp(p.y, 0.0, h);
				//	return length(p) - _Radius;
				//	//return length(p) - _Radius;//distance(p, _Centre) - _Radius;
				//}

				////------------------------------------------------------------------
				//float sphereHit(float3 p, float3 _Centre, float _Radius)
				//{
				//	float shapeValue = Shape(p, _Centre, _Radius);
				//	float displacementValue = displacement(p, _Radius);
				//	return shapeValue + displacementValue;
				//}

				float rand(float co)
				{
					return frac(sin(dot(co, float3(12.9898, 78.233, 45.5432))) * 43758.5453);
				}

				//------------------------------------------------------------------
				/// Sample texture to find strength of cloud
				//------------------------------------------------------------------
				float sampleTexture(float3 p, float3 _Centre, float _Radius)
				{
					float worldSize = 30.0f;
					float textureSize = 512.0f;// *p.y * 0.1f;

					float2 coordinates = float2((p.x / worldSize), (p.z / worldSize));

					coordinates += _Time.y * _CloudSpeed.xy * 0.05f ;
					float4 textureSample1 = tex2D(_NoiseTex, coordinates);

					coordinates = float2((p.x / worldSize), (p.y / worldSize));
					//coordinates += _Time.y * _CloudSpeed.zw * 0.05f ;
					float4 textureSample2 = tex2D(_NoiseTex, coordinates);

					float4 result = (textureSample1 + textureSample2) / 2.0f;
					//-- Take average colour intensity (Gray Scale)
					return (result.r + result.g + result.b) / 3.0f;
				}

				//------------------------------------------------------------------
				float raymarchHit(float3 _Centre, float _Radius, float3 position, float3 direction)
				{
					//-- Smaler a vlue means smaller clouds
					float minimumValue = 0.4f;

					for (int i = 0; i < STEPS; ++i)
					{
						float distance = sampleTexture(position, _Centre, _Radius);//sphereHit(position, _Centre, _Radius);

						if (distance < minimumValue)
						{
							return position.y;
						}

						position += direction * STEP_SIZE;
					}

					return MAX_FLOAT;
				}

				//------------------------------------------------------------------
				///
				//------------------------------------------------------------------
				fixed4 simpleLambert(fixed3 normal, float3 viewDirection)
				{
					fixed3 lightDir = _WorldSpaceLightPos0.xyz;	// Light direction
					fixed3 lightCol = _LightColor0.rgb;		// Light color

					fixed NdotL = max(dot(normal, lightDir), 0);
					fixed4 c;
					// Specular
					fixed3 h = (lightDir - viewDirection) / 2.;
					fixed s = pow(dot(normal, h), _SpecularPower) * _Gloss;
					c.rgb = _ObjectColor * lightCol * NdotL + s;
					c.a = 1;

					return c;
				}
				//------------------------------------------------------------------
				///
				//------------------------------------------------------------------
				float3 normal(float3 p, float3 _Centre, float _Radius)
				{
					const float eps = 0.01;

					return normalize
					(float3
						(sampleTexture(p + float3(eps, 0, 0), _Centre, _Radius) - sampleTexture(p - float3(eps, 0, 0), _Centre, _Radius),
							sampleTexture(p + float3(0, eps, 0), _Centre, _Radius) - sampleTexture(p - float3(0, eps, 0), _Centre, _Radius),
							sampleTexture(p + float3(0, 0, eps), _Centre, _Radius) - sampleTexture(p - float3(0, 0, eps), _Centre, _Radius))
					);
				}
				//------------------------------------------------------------------
				///
				//------------------------------------------------------------------
				fixed4 renderSurface(float3 p, float3 viewDirection, float3 _Centre, float _Radius)
				{
					float3 n = normal(p, _Centre, _Radius);
					return simpleLambert(n, viewDirection);
				}

				//------------------------------------------------------------------
				///
				//------------------------------------------------------------------
				fixed4 frag(v2f i) : SV_Target
				{
					// sample the texture
					fixed4 col = tex2D(_MainTex, i.uv);

					float radius = 0.5f;
					float distance = raymarchHit(i.objectWorldPosition, radius, i.vertexWorldPosition, i.viewDir);

					if (distance != MAX_FLOAT)
					{
						float interpolate = min(max(distance / 1.0f, 0.0f), 1.0f);

						col = lerp(_ObjectColorDark, _ObjectColor, interpolate);
						//col.a = lerp(1.0f, 0.0f, interpolate);
						//-- Lighting addition.
						//col *= renderSurface(i.vertexWorldPosition, i.viewDir, i.objectWorldPosition, radius);
						return col;
					}
					else
					{
						col *= _Color;
						return col;
					}
				}
			ENDCG
			}
	}
}
