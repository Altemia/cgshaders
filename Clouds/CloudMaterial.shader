﻿//------------------------------------------------------------------
///
//------------------------------------------------------------------
Shader "Custom/CloudMaterial"
{
	Properties
	{
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_SkyColour("_SkyColour", Color) = (0.5,0.5,0.5,1.0)
		_CloudTex("Cloud tex (RGB)", 2D) = "white" {}

		//-- Strengths
		_CloudSpread("_CloudSpread", Float) = 0.1
		_CloudSmooth("_CloudSmooth - Smoothness of edges", Float) = 0.1
		_CloudStrength("_CloudStrength", Float) = 0.1

		_CloudSpeed("Cloud Speed", Vector) = (0.5,0.5,0.5,1.0)
		_CloudColour("Cloud Colour", Color) = (0.5,0.5,0.5,1.0)
		_CloudDark("Cloud Dark", Color) = (0.5,0.5,0.5,1.0)
	}
	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"RenderType" = "Transparent"
		}
		LOD 200

		Pass
		{
			//ZWrite On
			Cull Off
			Fog { Mode Off }
			Blend SrcAlpha OneMinusSrcAlpha

		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			#include "Lighting.cginc"

			sampler2D _MainTex;
			sampler2D _CloudTex;
			float4 _MainTex_ST;
			float4 _CloudTex_ST;
			fixed4 _SkyColour;

			float _CloudSpread = 0.5f;
			float _CloudSmooth = 0.3f;
			float _CloudStrength = 0.5f;

			fixed4 _CloudColour;
			fixed4 _CloudDark;
			float4 _CloudSpeed;

			struct v2f
			{
				float4 vertex   : POSITION;
				float3 normal : NORMAL;
				half2 texcoord  : TEXCOORD0;
				half2 foam_texcoord : TEXCOORD1;
				float3 wPos : TEXCOORD2; // World position
				float4 color: COLOR;
			};

			float hash(float2 n)
			{
				return frac(sin(dot(n.xy,
					float2(1762.9898, 78.233)))
					* 43758.5453123);
			}

			float Noise3(float2 in_position)
			{
				float2 i = floor(in_position);
				float2 f = frac(in_position);

				// Four corners in 2D of a tile
				float a = hash(i);
				float b = hash(i + float2(1.0, 0.0));
				float c = hash(i + float2(0.0, 1.0));
				float d = hash(i + float2(1.0, 1.0));

				// Smooth Interpolation

				// Cubic Hermine Curve.  Same as SmoothStep()
				float2 u = f * f*(3.0 - 2.0*f);

				// Mix 4 coorners percentages
				return lerp(a, b, u.x) +
					(c - a)* u.y * (1.0 - u.x) +
					(d - b) * u.x * u.y;
			}

			v2f vert(appdata_full IN)
			{
				float _UVScrollingX = 0.0f;
				float _UVScrollingY = 0.0f;
				float time = _Time.y;

				v2f OUT;
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.normal.xyz = IN.normal * -1.0f;
				OUT.wPos = mul(unity_ObjectToWorld, IN.vertex).xyz;
				OUT.color = float4(1.0f, 1.0f, 1.0f, 0.0f);

				//Main texture UVs
				half2 mainTexcoords = IN.texcoord.xy;
				mainTexcoords.xy += half2(_UVScrollingX, _UVScrollingY) * time * 0.1;
				OUT.texcoord.xy = TRANSFORM_TEX(mainTexcoords.xy, _MainTex);
				OUT.foam_texcoord.xy = TRANSFORM_TEX(mainTexcoords.xy, _CloudTex);

				return OUT;
			}

			float sphereDistance(float3 p)
			{
				float3 center = float3(0, 0.0, 0.0f);
				float radius = 2.1f;
				return 0.0f;// distance(p, p) - radius;
			}

			fixed4 raymarch(float3 position, float3 direction)
			{
				float MIN_DISTANCE = 0.0f;
				int STEPS = 200;

				for (int i = 0; i < STEPS; ++i)
				{
					float distance = sphereDistance(position);
					if (distance <= MIN_DISTANCE)
					{
						return fixed4(1,1,1,1) * (i / (float)STEPS);
					}

					position += distance * direction;
				}
				return 0;
			}
			fixed4 simpleLambert(fixed3 normal)
			{
				fixed3 lightDir = _WorldSpaceLightPos0.xyz; // Light direction
				fixed3 lightCol = _LightColor0.rgb; // Light color

				fixed NdotL = max(dot(normal, lightDir), 0);
				fixed4 c;
				c.rgb = _CloudColour * lightCol * NdotL;
				c.a = 1;
				return c;
			}


			fixed4 frag(v2f IN) : COLOR
			{
				float4 _FoamSpeed = _CloudSpeed;
				half4 color = tex2D(_MainTex, IN.texcoord.xy);

				float sceneZ = SAMPLE_DEPTH_TEXTURE_PROJ(_CloudTex, UNITY_PROJ_COORD(IN.vertex));
				sceneZ = LinearEyeDepth(sceneZ);

				float partZ = IN.vertex.z;
				float depthDiff = 1.0f;// abs(sceneZ - partZ);
				float foamDepth = saturate(_CloudSpread * depthDiff);

				//Depth-based foam
				half2 foamUV = IN.foam_texcoord.xy;
				foamUV.xy += _Time.y *_FoamSpeed.xy * 0.05;
				fixed4 clouds = tex2D(_CloudTex, foamUV);
				
				foamUV.xy += _Time.y *_FoamSpeed.zw * 0.05;
				fixed4 clouds2 = tex2D(_CloudTex, foamUV);
				clouds = (clouds + clouds2) / 2;
			
				half foamTerm = (smoothstep(clouds.r - _CloudSmooth, clouds.r + _CloudSmooth, saturate(_CloudStrength - foamDepth)) * saturate(1.0f - foamDepth)) * _CloudColour.a;
				
				color = lerp(_SkyColour, lerp(_CloudColour, _CloudDark, 1.0f - foamTerm), foamTerm);

				color.a = lerp(_SkyColour.a, color.a, foamTerm);

				//color *= simpleLambert(-IN.normal);
				
				//float newAlpha = 1.0f;

				//color.a = mainTex.a * _Color.a;
				//color.a = lerp(color.a, _FoamColor.a, foamTerm);

				//-- Marching for depth.
				//float3 viewDirection = normalize(IN.wPos - _WorldSpaceCameraPos);
				//float3 worldPosition = IN.wPos;
				// = raymarch(worldPosition, viewDirection);

				//-- Horizontal Alpha fade.
				//float startOffset = 0.2f;
				//float endOffset = 1.0f - startOffset;
				//float x = IN.texcoord.x;

				//newAlpha *= max(min(lerp(0.0f, 1.0f, (x / startOffset)), 1.0f), 0.0f);
				//if (IN.texcoord.x >= endOffset)
				//{
				//	newAlpha *= max(lerp(1.0f, 0.0f, ((x - endOffset) / startOffset)), 0.0f);
				//}

				//color.a *= newAlpha;

				return color;
			}
		ENDCG
		}
	}
	FallBack "Diffuse"
}
