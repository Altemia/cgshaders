﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "custom/DayNightBoxShader"
{
	Properties
	{
		_Day("Environment Map", Cube) = "white" {}
		_Night("Environment Map", Cube) = "white" {}

		_t("Normalised Value", Range(0, 1)) = 0
	}

	SubShader
	{
		   Tags { "Queue" = "Background"  }

		   Pass {
			  ZWrite Off
			  Cull Off

			  CGPROGRAM
			  #pragma vertex vert
			  #pragma fragment frag

		// User-specified uniforms
		samplerCUBE _Day;
		samplerCUBE _Night;
		
		float _t;

		struct vertexInput {
		   float4 vertex : POSITION;
		   float3 texcoord : TEXCOORD0;
		};

		struct vertexOutput {
		   float4 vertex : SV_POSITION;
		   float3 texcoord : TEXCOORD0;
		};

		vertexOutput vert(vertexInput input)
		{
		   vertexOutput output;
		   output.vertex = UnityObjectToClipPos(input.vertex);
		   output.texcoord = input.texcoord;
		   return output;
		}

		fixed4 frag(vertexOutput input) : COLOR
		{
			fixed4 c = lerp(texCUBE(_Day, input.texcoord), texCUBE(_Night, input.texcoord), _t);
			return c;
		}
		ENDCG
	 }
	}
}