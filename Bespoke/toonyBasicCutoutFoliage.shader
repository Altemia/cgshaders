﻿//------------------------------------------------------------------
/// Toony Colors Pro+Mobile 2
/// (c) 2014-2018 Jean Moreno
///
/// - Edited at Storm Cloud games to add Foliage sway
//------------------------------------------------------------------

Shader "custom/toonyBasicCutoutFoliage"
{
	Properties
	{
	[TCP2HeaderHelp(BASE, Base Properties)]
		//TOONY COLORS
		_Color ("Color", Color) = (1,1,1,1)
		_HColor ("Highlight Color", Color) = (0.785,0.785,0.785,1.0)
		_SColor ("Shadow Color", Color) = (0.195,0.195,0.195,1.0)

		//DIFFUSE
		_MainTex ("Main Texture", 2D) = "white" {}
	[TCP2Separator]

		//TOONY COLORS RAMP
		[TCP2Header(RAMP SETTINGS)]

		_RampThreshold ("Ramp Threshold", Range(0,1)) = 0.5
		_RampSmooth ("Ramp Smoothing", Range(0.001,1)) = 0.1
	[TCP2Separator]

	[TCP2HeaderHelp(TRANSPARENCY)]
		//Alpha Testing
		_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
	[TCP2Separator]


		//Avoid compile error if the properties are ending with a drawer
		[HideInInspector] __dummy__ ("unused", Float) = 0

	//-- Wind
		_WindDirection("Wind Direction", Vector) = (1,1,1,1)
		_WindPower("Wind Power", Float) = 1

	//-- Graph
		_Frequency("Graph Frequency", Float) = 1
		_Amplitude("Graph Amplitude", Float) = 1
	}

	SubShader
	{
		Tags
		{
			"Queue"="AlphaTest"
			"IgnoreProjector"="True" 
			"RenderType"="TransparentCutout"
		}

		CGPROGRAM

		#pragma surface surf ToonyColorsCustom addshadow exclude_path:deferred exclude_path:prepass vertex:vert
		#pragma target 2.5

		//================================================================
		// VARIABLES

		fixed4 _Color;
		sampler2D _MainTex;
		fixed _Cutoff;

		#define UV_MAINTEX uv_MainTex

		struct Input
		{
			half2 uv_MainTex;
		};

		//-- Wind power
		float _WindPower;
		float4 _WindDirection;
		float _Frequency;
		float _Amplitude;

		//================================================================
		// CUSTOM LIGHTING

		//Lighting-related variables
		fixed4 _HColor;
		fixed4 _SColor;
		half _RampThreshold;
		half _RampSmooth;

		// Instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		//Custom SurfaceOutput
		struct SurfaceOutputCustom
		{
			half atten;
			fixed3 Albedo;
			fixed3 Normal;
			fixed3 Emission;
			half Specular;
			fixed Gloss;
			fixed Alpha;
		};

		inline half4 LightingToonyColorsCustom (inout SurfaceOutputCustom s, half3 viewDir, UnityGI gi)
		{
		#define IN_NORMAL s.Normal
	
			half3 lightDir = gi.light.dir;
		#if defined(UNITY_PASS_FORWARDBASE)
			half3 lightColor = _LightColor0.rgb;
			half atten = s.atten;
		#else
			half3 lightColor = gi.light.color.rgb;
			half atten = 1;
		#endif

			IN_NORMAL = normalize(IN_NORMAL);
			fixed ndl = max(0, dot(IN_NORMAL, lightDir));
			#define NDL ndl

			#define		RAMP_THRESHOLD	_RampThreshold
			#define		RAMP_SMOOTH		_RampSmooth

			fixed3 ramp = smoothstep(RAMP_THRESHOLD - RAMP_SMOOTH*0.5, RAMP_THRESHOLD + RAMP_SMOOTH*0.5, NDL);
		#if !(POINT) && !(SPOT)
			ramp *= atten;
		#endif
		#if !defined(UNITY_PASS_FORWARDBASE)
			_SColor = fixed4(0,0,0,1);
		#endif
			_SColor = lerp(_HColor, _SColor, _SColor.a);	//Shadows intensity through alpha
			ramp = lerp(_SColor.rgb, _HColor.rgb, ramp);
			fixed4 c;
			c.rgb = s.Albedo * lightColor.rgb * ramp;
			c.a = s.Alpha;

		#ifdef UNITY_LIGHT_FUNCTION_APPLY_INDIRECT
			c.rgb += s.Albedo * gi.indirect.diffuse;
		#endif


			return c;
		}

		void LightingToonyColorsCustom_GI(inout SurfaceOutputCustom s, UnityGIInput data, inout UnityGI gi)
		{
			gi = UnityGlobalIllumination(data, 1.0, IN_NORMAL);

			s.atten = data.atten;	//transfer attenuation to lighting function
			gi.light.color = _LightColor0.rgb;	//remove attenuation
		}

		//------------------------------------------------------------------
		/// Apply Sway - The sway is randomised so all objects sway independently.
		//------------------------------------------------------------------
		float3 ApplySway(float4 vertex, float2 in_texCoord1, float4 in_vertexColour)
		{
			float4 result = vertex;
			float yConstraint = 1.0f - in_vertexColour.a;

			if (in_vertexColour.a < 1.0f)
			{
				float3 objectWorldPos = mul(unity_ObjectToWorld, float4(0, 0, 0, 1));
				float distanceFromBottom = distance(vertex, float4(0, 0, 0, 1));

				float amplitude = _Amplitude;
				float frequency = _Frequency;

				float graphXAxis = (frequency * (_Time.y + objectWorldPos.x + objectWorldPos.z + in_texCoord1.x + in_texCoord1.y));
				float movementValue = _WindPower * yConstraint * (amplitude * cos(graphXAxis));

				result.xyz += (_WindDirection.xyz * movementValue);

				//float xPower = speed.x * yConstraint * (amplitude * cos(graphXAxis));
				//float zPower = speed.z * yConstraint * (amplitude * sin(graphXAxis));

				float3 newDirection = normalize(result.xyz - float3(0, 0, 0));
				//result.xyz = float3(0, 0, 0) + (newDirection * distanceFromBottom);

			}
			return result.xyz;
		}

		//------------------------------------------------------------------
		/// Vertex.
		//------------------------------------------------------------------
		void vert(inout appdata_full input)
		{
			//-- Apply Sway
			input.vertex.xyz = ApplySway(input.vertex, input.texcoord1, input.color);
		}

		//------------------------------------------------------------------
		/// SURFACE FUNCTION
		//------------------------------------------------------------------
		void surf(Input IN, inout SurfaceOutputCustom o)
		{
			fixed4 mainTex = tex2D(_MainTex, IN.UV_MAINTEX);
			o.Albedo = mainTex.rgb * _Color.rgb;
			o.Alpha = mainTex.a * _Color.a;
	
			//Cutout (Alpha Testing)
			clip (o.Alpha - _Cutoff);
		}

		ENDCG
	}

	Fallback "Diffuse"
	CustomEditor "TCP2_MaterialInspector_SG"
}
