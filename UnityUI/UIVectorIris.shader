﻿Shader "custom/UIVectorIris"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
        [HideInInspector] _Flip ("Flip", Vector) = (1,1,1,1)
        [PerRendererData] _AlphaTex ("External Alpha", 2D) = "white" {}
        [PerRendererData] _EnableExternalAlpha ("Enable External Alpha", Float) = 0

		_Scale("Scale", float) = 1
    }

    SubShader
    {
        Tags
        {
            "Queue"="Overlay"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
		ZTest Always
        ZWrite Off
		Fog { Mode Off}

		Blend One OneMinusSrcAlpha

        Pass
        {
        CGPROGRAM
			#pragma vertex vert
            
#pragma fragment SpriteFrag
            #pragma target 2.0
            #pragma multi_compile_instancing
            #pragma multi_compile _ PIXELSNAP_ON
            #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
            #include "UnitySprites.cginc"

			float4 _MainTex_ST;
			float _Scale;

			v2f vert(appdata_full IN)
			{
				_Scale = max(_Scale, 0);

				v2f OUT;
				//-- Vertex Positions
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				//-- Texture Coordinates
				OUT.texcoord = IN.texcoord;

				float4x4 scaleMat;
				scaleMat[0][0] = 1.0;
				scaleMat[0][1] = 0.0;
				scaleMat[0][2] = 0.0;
				scaleMat[0][3] = 0.0;
				scaleMat[1][0] = 0.0;
				scaleMat[1][1] = 1.0;
				scaleMat[1][2] = 0.0;
				scaleMat[1][3] = 0.0;
				scaleMat[2][1] = 0.0;
				scaleMat[2][1] = 0.0;
				scaleMat[2][2] = 1.0;
				scaleMat[2][3] = 0.0;
				scaleMat[3][0] = 0.0;
				scaleMat[3][1] = 0.0;
				scaleMat[3][2] = 0.0;
				scaleMat[3][3] = 1.0;

				float4 worldPos = mul(unity_ObjectToWorld, float4(0, 0, 0, 1));
				float4 rightWorldCornerPos = mul(unity_ObjectToWorld, float4(1, 1, 1, 1));
				float4 leftWorldCornerPos = mul(unity_ObjectToWorld, float4(1, 1, 1, -1));

				float4 objectVertexPos = mul(unity_WorldToObject, OUT.vertex);
				float4 objectPos = mul(unity_WorldToObject, worldPos);
				float4 rightCornerPos = mul(unity_WorldToObject, rightWorldCornerPos);
				float4 leftCornerPos = mul(unity_WorldToObject, leftWorldCornerPos);

				float maxDistance = distance(objectPos, rightCornerPos);

				float4 maximumDistance = objectPos + float4(1, 1, 1, 0);
				float currentDistance = distance(objectVertexPos, objectPos);

				float fraction = (currentDistance / maximumDistance);

				float4 newPosition = UnityObjectToClipPos(IN.vertex * _Scale);
				float newDistance = distance(newPosition, objectPos);

				if (newDistance > currentDistance || (newDistance < currentDistance && fraction < 1.0f))
				{
					OUT.vertex = newPosition;
				}

				//-- Colour
				fixed4 color = IN.color;
				OUT.color = color;
				OUT.color.a = color.a;

				return OUT;
			}
        ENDCG
        }
    }
}
