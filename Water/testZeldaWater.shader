﻿//------------------------------------------------------------------
///
//------------------------------------------------------------------
Shader "FX/daveTestZeldaWater"
{
	Properties
	{
		_WaveTex("Wave Noise", 2D) = "white" {}
		_FoamTex("Foam (RGB)", 2D) = "white" {}
		_TideGradient("Tide Gradient", 2D) = "white" {}

		_UVScrollingX("X Speed", Float) = 0.1
		_UVScrollingY("Y Speed", Float) = 0.1

		_WaterColour1("_WaterColour1", Color) = (1,1,1,1)
		_WaterColour2("_WaterColour2", Color) = (1,1,1,1)

		_FoamColour("_FoamColour", Color) = (1,1,1,1)

		_DistanceColour("_DistanceColour", Color) = (1,1,1,1)

		_WaveSpeed("Wave Speed", Vector) = (1,1,1,1)

		_FoamSpread("Foam Spread", Range(0.01,5)) = 2
		_FoamStrength("Foam Strength", Range(0.01,1)) = 0.8
		_FoamSmooth("Foam Smoothness", Range(0,0.5)) = 0.02
		_FoamSpeed("Foam Speed", Vector) = (2,2,2,2)
	}

	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"RenderType" = "Transparent"
		}

		ZWrite On
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			Tags
			{
				"LightMode" = "ForwardBase"
			}

		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#define M_2PI 6.283185307
			#define M_6PI 18.84955592

			#define HAS_FOAM

			#include "UnityCG.cginc"

			//------------------------------------------------------------------
			/// Output Vertex Data structure
			//------------------------------------------------------------------
			struct v2f
			{
				float4 pos : SV_POSITION;

				float4 screenPos : TEXCOORD0;
				float3 worldPos : TEXCOORD2;
				fixed3 vertexWorldPos : COLOR;

				float2 texcoord  : TEXCOORD3;
				float2 foam_texcoord : TANGENT;
				float2 wave_texcoord : NORMAL;

				float3 viewDir : TEXCOORD1;

				float depth : DEPTH;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			sampler2D _FoamTex;
			float4 _FoamTex_ST;

			sampler2D _WaveTex;
			float4 _WaveTex_ST;

			sampler2D _TideGradient;

			half _UVScrollingX;
			half _UVScrollingY;

			fixed4 _WaterColour1;
			fixed4 _WaterColour2;
			fixed4 _FoamColour;
			fixed4 _DistanceColour;

			half4 _WaveSpeed;

			half4 _FoamSpeed;
			half _FoamSpread;
			half _FoamStrength;
			half _FoamSmooth;

			uniform sampler2D _CameraDepthTexture; // automatically set up by Unity. Contains the scene's depth buffer
			
			//------------------------------------------------------------------
			/// Vertex Tide along the sbhore.
			//------------------------------------------------------------------
			float3 ApplyVertexTide(float4 vertex, float3 in_worldPos, float2 in_texCoords)
			{
				float4 result = vertex;

				float speed = 0.15f;
				float yConstraint = vertex.z > (in_worldPos.z - 10.3f) ? 0.0f : 1.0f;
				float amplitude = 0.5f;
				float frequency = 1.15f;

				float yPower = speed * yConstraint * (amplitude * sin(frequency * _Time.y) + 0.0f);
				result.y += yPower;

				return result.xyz;
			}

			//------------------------------------------------------------------
			/// Vertex method.
			//------------------------------------------------------------------
			v2f vert(appdata_full IN)
			{
				v2f OUT;

				//-- Apply a moving in and out tide.
				IN.vertex.xyz = ApplyVertexTide(IN.vertex, UnityObjectToClipPos(IN.vertex), IN.texcoord);

				//-- Set Out Values.
				OUT.pos = UnityObjectToClipPos(IN.vertex);

				OUT.texcoord = TRANSFORM_TEX(IN.texcoord, _MainTex);
				OUT.foam_texcoord = TRANSFORM_TEX(IN.texcoord, _FoamTex);
				OUT.foam_texcoord += half2(_UVScrollingX, _UVScrollingY) * _Time.y * 0.1;
				OUT.wave_texcoord.xy = TRANSFORM_TEX(IN.texcoord, _WaveTex);

				OUT.worldPos = mul(unity_ObjectToWorld, float4(0, 0, 0, 1)).xyz;
				OUT.vertexWorldPos = mul(unity_ObjectToWorld, IN.vertex).xyz;
				OUT.viewDir = WorldSpaceViewDir(IN.vertex);

				//-- Screen Position
				OUT.screenPos = ComputeScreenPos(OUT.pos);
				OUT.screenPos.z = -mul(UNITY_MATRIX_MV, IN.vertex).z;

				OUT.depth = -mul(UNITY_MATRIX_MV, IN.vertex).z *_ProjectionParams.w;

				return OUT;
			}

			//------------------------------------------------------------------
			/// Apply Alpha Fade along the shore.
			//------------------------------------------------------------------
			float4 ApplyShoreFade(float4 fragColour, float4 in_sPos, float in_depthDiff)
			{
				float4 result = fragColour;

				float interpolate = min(in_depthDiff * 1.5f, 1.0f);

				result.a = lerp(0.0f, 1.0f, interpolate);

				return result;
			}

			//------------------------------------------------------------------
			/// Apply Foam along the shore.
			//------------------------------------------------------------------
			float4 ApplyShoreFoam(float4 fragColour, v2f IN, float in_depthDiff)
			{
				float4 result = fragColour;

				//Depth-based foam
				half2 foamUV = IN.foam_texcoord.xy;
				foamUV.xy += _Time.y.xx*_FoamSpeed.xy*0.05;
				fixed4 foam = tex2D(_FoamTex, foamUV);
				foamUV.xy += _Time.y.xx*_FoamSpeed.zw*0.05;
				fixed4 foam2 = tex2D(_FoamTex, foamUV);
				foam = (foam + foam2) / 2;
				float foamDepth = saturate(_FoamSpread * in_depthDiff);
				half foamTerm = (smoothstep(foam.r - _FoamSmooth, foam.r + _FoamSmooth, saturate(_FoamStrength - foamDepth)) * saturate(1 - foamDepth)) * _FoamColour.a;

				//-- Apply Shore Foam
				result.rgb = lerp(result.rgb, _FoamColour, foamTerm);

				return result;
			}

			float4 OceanEffect(float4 fragColour, v2f IN, float in_depthDiff)
			{
				float4 result = fragColour;
				float3 cpos = _WorldSpaceCameraPos; // Camera position

				// Parallax height distortion with two directional waves at
				// slightly different angles.
				float power = 0.005f;//0.025f
				float2 a = power * IN.viewDir.xz / IN.viewDir.y; // Parallax offset
				float h = sin(IN.wave_texcoord.x + _Time.y); // Height at UV
				IN.wave_texcoord += a * h;
				h = sin(0.841471 * IN.wave_texcoord.x - 0.540302 * IN.wave_texcoord.y + _Time.y);
				IN.wave_texcoord += a * h;

				// Texture distortion
				//float d1 = fmod(IN.wave_texcoord.x + IN.wave_texcoord.y, M_2PI);
				//float d2 = fmod((IN.wave_texcoord.x + IN.wave_texcoord.y + 0.25) * 1.3, M_6PI);
				//d1 = _Time.y * 0.1 + d1;
				//d2 = _Time.y * 0.6 + d2;
				//float2 dist = float2(
				//	sin(d1) * 0.05 + sin(d2) * 0.05,
				//	cos(d1) * 0.05 + cos(d2) * 0.05
				//	);

				float _CloudSpread = 0.11;
				float foamDepth = saturate(_CloudSpread);
				float _CloudSmooth = 0;
				float _CloudStrength = 0.44;

				//Depth-based foam
				half2 foamUV = IN.wave_texcoord.xy;
				foamUV.xy += _Time.y *_WaveSpeed.xy * 0.05;
				fixed4 foam = tex2D(_WaveTex, foamUV);

				foamUV.xy += _Time.y *_WaveSpeed.zw * 0.05;
				fixed4 foam2 = tex2D(_WaveTex, foamUV);
				foam = (foam + foam2) / 2;

				float4 newFoamColour = _FoamColour;

				half foamTerm = (smoothstep(foam.r - _CloudSmooth, foam.r + _CloudSmooth, saturate(_CloudStrength - foamDepth)) * saturate(1.0f - foamDepth));//  *newFoamColour.a;

				float vertexDistance = abs(distance(cpos, IN.vertexWorldPos.xyz));
				if (vertexDistance >= 250.0)
				{
					float difference = ((vertexDistance - 250.0f) / 200.0f);
					newFoamColour.a = lerp(_FoamColour.a, 0.0f, difference);
				}
				else
				{
					float difference = vertexDistance * 0.005f;

					newFoamColour.a = lerp(0.0f, newFoamColour.a, difference);
				}

				foamTerm += 1.0f - min(max(newFoamColour.a, 0.0f), _FoamColour.a);
				result.rgb = lerp(newFoamColour.rgb, result.rgb, min(max(foamTerm, 0.0f), 1.0f));

				//float interpolate = lerp(foamTerm, 0.0, intensityFactor);
				//result = lerp(newFoamColour.rgba, result, min(max(interpolate, 0), 1.0));

				/*float2 offset = float2(0.0, 0.0);
				float2 uv = (IN.pos.xz + offset) / _ScreenParams.xy;
				float3 cpos = _WorldSpaceCameraPos; // Camera position
				float3 cdir = IN.viewDir;

				const float3 oceanPos = float3(0, 1.0f,0);
				float vertexDistance = abs(distance(cpos, IN.vertexWorldPos.xyz));
				float objectDist = distance(cpos, oceanPos) * 0.05f;//(-dot(cpos, oceanPos) / dot(cdir, oceanPos)) * 5.0f;
				float3 pos = cpos + objectDist * cdir;

				//-- Scale Surf Colours as Increase in distance from the camera.
				float4 surfReflectionColour = _WaterColour2;
				surfReflectionColour.a = lerp(1.0f, 0.05f, min(vertexDistance * 0.05f, 1.0));

				//-- Fade out the ripples as you approach camera.
				float4 foamColour = _FoamColour;
				//foamColour.a = lerp(0.1f, 1.0f, min(vertexDistance * 0.008f, 1.0));

				for (int y = 0; y < 2; ++y)
				{
					for (int x = 0; x < 2; ++x)
					{
						//-- Water
						float3 waterColour = water(pos.xz, IN.viewDir, surfReflectionColour, foamColour);

						waterColour = lerp(waterColour, _DistanceColour.rgb, min(vertexDistance * 0.002f, 1.0));

						result.rgb += waterColour * float3(0.25, 0.25, 0.25);
					}
				}*/

				return result;
			}

			//------------------------------------------------------------------
			/// Apply Fragment Tides that move towards the shore periodically.
			//------------------------------------------------------------------
			float4 ApplyFragmentTides(float4 fragColour, v2f IN, float in_depthDiff)
			{
				float4 result = fragColour;

				float _FoamStrength = 1.3f;
				//-- Add a less rigid edge to the foam.
				half3 bump1 = UnpackNormal(tex2D(_FoamTex, IN.foam_texcoord)).rgb;
				half3 bump2 = UnpackNormal(tex2D(_FoamTex, IN.foam_texcoord)).rgb;
				half3 bump = (bump1 + bump2) * 0.5;

				float intensityFactor = 1 - saturate(in_depthDiff / _FoamStrength);
				half3 foamGradient = 1 - tex2D(_TideGradient, float2(intensityFactor - _Time.y * 0.15, 0) + bump.xy * 0.15);
				float2 foamDistortUV = bump.xy * 0.002;
				half3 foamColor = tex2D(_FoamTex, IN.foam_texcoord + foamDistortUV).rgb;
				half foamLightIntensity = 0.4f;// saturate((-_WorldSpaceLightPos0.z + 0.2) * 0.8);

				result.rgb += foamGradient * intensityFactor * foamColor * foamLightIntensity;

				return result;
			}

			//------------------------------------------------------------------
			/// Fragment method.
			//------------------------------------------------------------------
			float4 frag(v2f IN) : SV_Target
			{
				float4 fragColour = _WaterColour1;

				//-- Shore Line
				float sceneZ = SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(IN.screenPos));
				sceneZ = LinearEyeDepth(sceneZ);

				float partZ = IN.screenPos.z;
				float depthDiff = abs(sceneZ - partZ);

				//-- Ocean wave random effect.
				fragColour = OceanEffect(fragColour, IN, depthDiff);

				//-- Apply Shore Foam
				//fragColour = ApplyShoreFoam(fragColour, IN, depthDiff);

				//-- Apply Moving tides of foam.
				fragColour = ApplyFragmentTides(fragColour, IN, depthDiff);

				//-- Apply fade as reach shore
				fragColour = ApplyShoreFade(fragColour, IN.screenPos, depthDiff);

				return fragColour;
			}
		ENDCG
		}
	}
}