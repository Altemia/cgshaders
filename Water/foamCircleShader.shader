﻿//------------------------------------------------------------------
///
//------------------------------------------------------------------
Shader "FX/foamCircleShader"
{
	Properties
	{
		_FoamTex("Foam (RGB)", 2D) = "white" {}

		_BumpTex("_BumpTex", 2D) = "white" {}

		_AlphaWrippleCutOff("_AlphaWrippleCutOff", Range(0,1)) = 1

		_UVWripple1X("_UVWripple1X", Float) = 0.1
		_UVWripple1Y("_UVWripple1Y", Float) = 0.1

		_UVWripple2X("_UVWripple2X", Float) = 0.1
		_UVWripple2Y("_UVWripple2Y", Float) = 0.1

		_WaterColour1("_WaterColour1", Color) = (1,1,1,1)

		_FoamColour("_FoamColour", Color) = (1,1,1,1)
	}

	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"RenderType" = "Transparent"
		}

		ZWrite On
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			Tags
			{
				"LightMode" = "ForwardBase"
			}

		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#define M_2PI 6.283185307
			#define M_6PI 18.84955592

			#define HAS_FOAM

			#include "UnityCG.cginc"

			//------------------------------------------------------------------
			/// Output Vertex Data structure
			//------------------------------------------------------------------
			struct v2f
			{
				float4 pos : SV_POSITION;

				float4 screenPos : TEXCOORD0;
				float3 worldPos : TEXCOORD2;
				fixed3 vertexWorldPos : TEXCOORD1;

				float2 texcoord  : TEXCOORD3;
				float2 foam_texcoord : TANGENT;

				float3 colour : COLOR;

				float depth : DEPTH;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			sampler2D _FoamTex;
			float4 _FoamTex_ST;

			sampler2D _BumpTex;
			float4 _BumpTex_ST;

			sampler2D _TideGradient;

			half _UVScrollingX;
			half _UVScrollingY;

			half _UVWripple1X;
			half _UVWripple1Y;

			half _UVWripple2X;
			half _UVWripple2Y;

			fixed4 _WaterColour1;
			fixed4 _WaterColour2;
			fixed4 _FoamColour;

			float _AlphaWrippleCutOff;

			uniform sampler2D _CameraDepthTexture; // automatically set up by Unity. Contains the scene's depth buffer
			
			//------------------------------------------------------------------
			/// Vertex method.
			//------------------------------------------------------------------
			v2f vert(appdata_full IN)
			{
				v2f OUT;

				//-- Set Out Values.
				OUT.pos = UnityObjectToClipPos(IN.vertex);

				OUT.colour = IN.color;

				OUT.texcoord = TRANSFORM_TEX(IN.texcoord, _MainTex);
				OUT.foam_texcoord = TRANSFORM_TEX(IN.texcoord, _FoamTex);
				//OUT.foam_texcoord += half2(_UVScrollingX, _UVScrollingY) * _Time.y * 0.1;
								
				OUT.worldPos = mul(unity_ObjectToWorld, float4(0, 0, 0, 1)).xyz;
				OUT.vertexWorldPos = mul(unity_ObjectToWorld, IN.vertex).xyz;

				//-- Screen Position
				OUT.screenPos = ComputeScreenPos(OUT.pos);
				OUT.screenPos.z = -mul(UNITY_MATRIX_MV, IN.vertex).z;

				OUT.depth = -mul(UNITY_MATRIX_MV, IN.vertex).z *_ProjectionParams.w;

				return OUT;
			}

			//------------------------------------------------------------------
			/// Apply Fragment Tides that move towards the shore periodically.
			//------------------------------------------------------------------
			float4 ApplyFragmentTides(float4 fragColour, v2f IN, float in_depthDiff)
			{
				float4 result = fragColour;
				result.a = _WaterColour1.a;
				float4 newFoamColour = result;

				float _FoamStrength = 2.5f;
				half3 bump = half3(0.2, 0.3, 0.5);

				in_depthDiff = distance(IN.vertexWorldPos, IN.worldPos);

				//float intensityFactor = 1.0 -saturate(in_depthDiff / _FoamStrength);
				//half4 foamGradient = half4(1, 1, 1, 1);
				//foamGradient.rgb = 1 - tex2D(_TideGradient, float2(intensityFactor + _Time.y * 0.15, 0));

				float4 newColourRedChannel = result;
				float4 newColourGreenChannel = result;

				float edgeAlphaCutOff = max(_AlphaWrippleCutOff - 0.1f, 0.0f);

				float2 wripple1TexCoords = IN.foam_texcoord + half2(_UVWripple1X, _UVWripple1Y) * _Time.y * 0.1;
				fixed4 effectChannel = tex2D(_BumpTex, wripple1TexCoords);			
				float redInterpolate = effectChannel.r * (IN.colour.r + edgeAlphaCutOff);

				float maximum = _AlphaWrippleCutOff;

				float2 wripple2TexCoords = IN.foam_texcoord + half2(_UVWripple2X, _UVWripple2Y) * _Time.y * 0.1;
				effectChannel = tex2D(_BumpTex, wripple2TexCoords);
				float greenInterpolate = effectChannel.g * (IN.colour.r + edgeAlphaCutOff);

				if (redInterpolate > maximum ||
					greenInterpolate > maximum)
				{
					result = _FoamColour;
				}

				return result;
			}

			//------------------------------------------------------------------
			/// Fragment method.
			//------------------------------------------------------------------
			float4 frag(v2f IN) : SV_Target
			{
				float4 fragColour = _WaterColour1;

				//-- Shore Line
				float sceneZ = SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(IN.screenPos));
				sceneZ = LinearEyeDepth(sceneZ);

				float partZ = IN.screenPos.z;
				float depthDiff = abs(sceneZ - partZ);

				//-- Apply Moving tides of foam.
				fragColour = ApplyFragmentTides(fragColour, IN, depthDiff);

				return fragColour;
			}
		ENDCG
		}
	}
}